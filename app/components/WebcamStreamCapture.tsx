'use client';

import React, { useState } from 'react';
import Webcam from 'react-webcam';

const VideoPlayer = ({ videoBlob }: { videoBlob: Blob }) => {
  const videoUrl = URL.createObjectURL(videoBlob);

  return (
    <video controls className="h-40 inline-block rounded-xl p-2">
      <source src={videoUrl} type="video/webm" />
      Your browser does not support the video tag.
    </video>
  );
};

const Picture = (src: string) => {
  return (
    // eslint-disable-next-line @next/next/no-img-element
    <img
      src={src}
      className="m-4 rounded-xl h-18 hover:opacity-50"
      alt=""
      key={src}
    />
  );
};

const WebcamStreamCapture = () => {
  const webcamRef = React.useRef<Webcam>(null);
  const mediaRecorderRef = React.useRef<MediaRecorder>();

  const [capturing, setCapturing] = React.useState(false);
  const [recordedChunks, setRecordedChunks] = React.useState<Blob[]>([]);

  const [images, setimages] = useState<string[]>([]);

  const captureScreenshot = React.useCallback(() => {
    const imageSrc = webcamRef.current?.getScreenshot();

    if (imageSrc) setimages((i) => [...i, imageSrc]);
  }, [webcamRef]);

  const handleDataAvailable = React.useCallback(
    ({ data }: { data: Blob }) => {
      if (data.size > 0) {
        setRecordedChunks((prev) => [...prev, data]);
      }
    },
    [setRecordedChunks]
  );

  const handleStartCaptureClick = React.useCallback(() => {
    setCapturing(true);

    if (!webcamRef.current) return;

    mediaRecorderRef.current = new MediaRecorder(
      webcamRef.current.stream as MediaStream,
      {
        mimeType: 'video/webm',
      }
    );
    mediaRecorderRef.current.addEventListener(
      'dataavailable',
      handleDataAvailable
    );
    mediaRecorderRef.current.start();
  }, [handleDataAvailable]);

  const handleStopCaptureClick = React.useCallback(() => {
    mediaRecorderRef.current?.stop();
    setCapturing(false);
  }, []);

  const handleDownload = React.useCallback(() => {
    if (recordedChunks.length) {
      const blob = new Blob(recordedChunks, {
        type: 'video/webm',
      });
      const url = URL.createObjectURL(blob);
      const a = document.createElement('a');
      document.body.appendChild(a);

      a.href = url;
      a.download = 'react-webcam-stream-capture.webm';
      a.click();
      window.URL.revokeObjectURL(url);
      setRecordedChunks([]);
    }
  }, [recordedChunks]);

  const genericButtonClasses = 'rounded-full p-4 m-2 text-white font-semibold';

  return (
    <>
      <div className="flex flex-nowrap overflow-x-scroll w-full h-44">
        {recordedChunks.map((c) => {
          return <VideoPlayer videoBlob={c} key={c.size} />;
        })}
      </div>
      <div className="flex flex-nowrap overflow-x-scroll w-full h-28">
        {images.map(Picture)}
      </div>
      <div className="rounded-xl overflow-hidden">
        <Webcam
          className="webcam"
          audio={true}
          muted={true}
          type="mp4"
          screenshotFormat="image/jpeg"
          width={1280}
          height={720}
          ref={webcamRef}
        />
      </div>
      <div className="flex flex-row">
        <button
          className={`${genericButtonClasses} bg-amber-400`}
          onClick={captureScreenshot}
        >
          Capture photo
        </button>
        {capturing ? (
          <button
            className={`${genericButtonClasses} bg-red-400`}
            onClick={handleStopCaptureClick}
          >
            Stop Capture
          </button>
        ) : (
          <button
            className={`${genericButtonClasses} bg-green-400`}
            onClick={handleStartCaptureClick}
          >
            Start Capture
          </button>
        )}
        <button
          className={`${genericButtonClasses} bg-blue-400`}
          onClick={handleDownload}
          disabled={!recordedChunks.length}
        >
          Download
        </button>
      </div>
    </>
  );
};

export default WebcamStreamCapture;
