// import Image from 'next/image';
import WebcamStreamCapture from './components/WebcamStreamCapture';

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-6">
      <WebcamStreamCapture />
    </main>
  );
}
